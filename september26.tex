\documentclass{article}
\usepackage[utf8]{inputenc}

\title{STA257 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 26 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\newtheorem*{notation}{Notation}

\begin{document}

\maketitle

Note: since the instructor has provided mostly complete slides during the lecture, many of the sentences here have been taken directly from the slides since there was little time to paraphrase.

\section*{Continuous random variables}
A continuous random variable is a variable that can take a value within a range of real numbers. Examples of continuous random variables include
\begin{enumerate}
  \item How long it takes you to walk to school
  \item Canada's current account (trade balance) \footnote{Some are rational numbers but these distinctions are often ignored}
  \item How far you walk today
  \item Your weight next September
  \item The electricity consumed by a restaurant tomorrow
\end{enumerate}
Last week we learned the probability \underline{mass} function. This week we'll define the probability \underline{density} function (\textbf{pdf}).

Recall that, in physics, density is mass over volume. That is, density describes how much mass is in a certain volume. Similarly, a pdf describes ``how much probability'' is contained in a unit-sized region: this region could be a volume of 1 milliliter, an area of 1 square centimeter, a length of 1 meter, anything that falls in the range of our continuous random variable. Oftentimes we'll use a dimensionless interval of unit length, either closed (\((0, 1)\)) or open (\([0, 1]\)).

\begin{definition}
A probability density function of a random variable \(X\) is a function \(f(x)\) such that, for any \(a < b\),
\[P(a < X < b) = \int_a^bf(x)dx\]
satisfying the following properties:
\begin{itemize}
  \item \(f(x)\) is piecewise continuous.
  \item \(f(x) \geq 0\). We don't talk about negative probabilities, though these are used in other disciplines, often by physicists.
  \item It has to ``sum up to 1,'' i.e.
  \[\int_{-\infty}^\infty f(x)dx = 1\]
\end{itemize}
\end{definition}

\section*{Motivation for a pdf}
In our model of your classmate's commute, we assumed the duration was between 5 and 7 minutes. What ifwe wanted to have a model which didn't round to the nearest integer, but instead included commuting times up to arbitrary precision. A continuous random variable can help answer that: we could, instead of a bar graph, have a smooth function defined at every point in the interval.

Note that we can go to a pmf from a pdf by integrating over regular (or potentially non-regular) intervals. The pdf gives us more flexibility however, because if our question was, say, how likely it was that our classmate would arrive in between 4.5 and 5 minutes, we could integrate as desired. Furthermore, the probability of ariving after \textit{exactly} 5 minutes would be zero, since it is infinitesimally small.

\section*{Cumulative Distribution Function}
\begin{definition}
  The cumulative distribution function (\textbf{cdf}) isdefined the same as in the case of discrete random variables, with a sum replacing the integral:
  \[F(x) = P(X \leq x) = \int_{-\infty}^xf(x)dx\]
\end{definition}
Note that \(f(x)\) is \textit{not} a probability, it's a probability \textit{density}, and hence, for example, can go above 1.

\section*{The \(p^{th}\) quantile}
\begin{definition}
For any continuous random variable with a strictly increasing cdf \(F\), the \(p^{th}\) of \(F\) is defined as \(F^{-1}(p)\)
\end{definition}
Common examples of quantiles include:
\begin{itemize}
  \item Percentiles: e.g. the \(98^{th}\) percentile is the \(0.98^{th}\) quantile. You simply use a factor of 100
  \item Lower quartile (a.k.a the first quartile): the \(0.25^{th}\) quantile.
  \item Upper quartile (a.k.a. the third quartile): the \(0.75^{th}\) quantile.
  \item Median: the \(0.5^{th}\) quantile.
\end{itemize}

\section*{Comparison of Discrete and Continuous Random Variables}

\begin{figure}
  \begin{tabular}{ccc}
    \hline
    Property & Discrete r.v. & Continuous r.v. \\
    \hline
    Probability distribution & pmf & pdf \\
    Prob. dist. notation in Rice & \(p(k)\) & \(f(x), g(t), f(u)\), etc. \\
    Sample space, \(\Omega\) & Intuitive & Harder to understand \\
    \(P(X = x)\) & \(\in [0, 1]\) & \(0\)
  \end{tabular}
  \caption{A table comparing the differences between continuous and directre random variables}
  \label{difs}
\end{figure}

For both discrete and continuous random variables,
\begin{itemize}
  \item Quantiles are \(x_p = \min_xF(x) \geq p\)
  \item Notions of independence, provided we develop our definition from Chapter 1
  \item Notions of hierarchy, i.e. some classes of random variable are special cases of others
  \item Can be defined on various types of intervals
  \item \(F(b) - F(a) = P(X \leq b) - P(X \leq a) = P(a < x \leq b)\)
  \item Other properties of the cdf
\end{itemize}

Some differences can be found in Figure \ref{difs}.

\subsection*{Different types of pdf intervals}
\begin{itemize}
  \item \(f: \reals \to \reals^+\)
  \item \(f: (-\infty, B] \to \reals^+\)
  \item \(f: [A, B] \to \reals^+\)
  \item \(f: [A, \infty) \to \reals^+\)
  \item \(f: (-\infty, B) \to \reals^+\)
  \item \(f: (A, B) \to \reals^+\)
  \item \(f: (A, \infty) \to \reals^+\)
\end{itemize}

\subsection*{Properties of the cdf for any r.v.}

We know a cdf is such that \(F(x) = P(X \leq x)\). Interestingly, any function with the following four properties is a cdf:
\begin{enumerate}
  \item \(\lim_{x \to -\infty}F(x) = 0\)
  \item \(\lim_{x \to \infty}F(x) = 1\)
  \item \(F(x)\) is nondecreasing: \(\forall x, y \in \reals, x > y \implies F(x) \geq F(y)\)
  \item \(F(x)\) is right-continuous:
  \[\lim_{h \to 0^+}F(x + h) = F(x)\]
\end{enumerate}
For any such function, we can find some random variable whose cdf is that function.

\section*{The uniform density}
Looking at the pdf of your commute-time random variable, most of the commute times fall in a three-minute region between 4.5 and 7.5 minutes. One naive approximation of the pdf is
\[f(x) = \left\{\begin{array}{cc}
  \frac{1}{3} & \text{if } x \in [4.5, 7.5] \\
  0 & \text{otherwise}
\end{array}\right.\]
In general,
\begin{definition}
  FOr \(a < b \in \reals\), we define the \underline{uniform density} or the \underline{continuous uniform distribution}
\[f(x) = \left\{\begin{array}{cc}
  \frac{1}{b - a} & \text{if } x \in [a, b] \\
  0 & \text{otherwise}
\end{array}\right.\]
When the outcomes of a discrete random variable are all equally likely, we can refer to its pmf as the \underline{discrete uniform distribution}
\end{definition}
Examples of the discrete uniform distribution include:
\begin{itemize}
  \item A Bernoulli trial with \(p = 1/2\)
  \item A "multinoulli" trial with \(p = 1/n\) (e.g. rolling a 6 sided die (\(1/6\)) or a d20 (\(1/20\))
  \item The chance that you'd win the class lottery
\end{itemize}

Back to the uniform desntiy, does \(f(x)\) meet all the criteria of a pdf? Yes!
\begin{itemize}
  \item Clearly, \(f(x)\) is piecewise continuous on the intervals \((-\infty, a), [a, b], (b, \infty)\)
  \item \(b - a > 0 \implies \frac{1}{b - a} > 0\)
  \item
  \[\int_{-\infty}^\infty f(x)dx = \int_a^b\frac{1}{b - a}dx = \left.\frac{x}{b - a}\right|_{a}^b = \frac{b - a}{b - a} = 1\]
\end{itemize}

\section*{Memoryless Distributions}
As mentioned in Week 1, the probability that you'll live until exactly one month from today varies depending on your age. For some things, this almost isn't the case, for example, the probabilities of failure per unit time for certain types of lightbulb is approximately constant.

Suppose that whether the lightbulb has been shining for one month or ten, the probability that it will fail per unit time \textit{is} constant. It's as if it has no \textit{memory} of the time it has shone. Among our discrete distributions, an analog would be the geometric distribution: whether sampling for the first time or the tenth, the probability of succeeding on the next trial is a constant, \(p\). It turns out that the geometric and exponential distributions are the \textit{only} memoryless distributions.

\subsection*{The Exponential Distribution}
It's possible to show \footnote{We won't show that memoryless \(\implies\) exponential, but we will prove the converse that exponential \(\implies\) memoryless} that the exponential distribution is the only memoryless continuous probability distribution.
\begin{definition}
  The exponential distribution has pdf given by
  \[f(x) = \lambda e^{-\lambda x}\]
  for some \(\lambda \in \reals^+\).
\end{definition}

\subsection*{The cdf of the exponential distribution}
Besides the uniform density, the exponential distribution is the only pdf we're considering today that has an easy to evaluate expression for it's cdf: for \(x \geq 0\),
\[F(x) = \int_{-\infty}^x\lambda e^{-\lambda u}du = 1 - e^{-\lambda x}\]
Some applications include:
\begin{enumerate}
  \item Checking if a distribution is memoryless (by seeing if we can fit it to the exponential distribution)
  \item Choosing \textit{how often} we should inspect devices for failure, e.g. how well is your fire extinguisher protecting you if you aren't chacking the pressure often.
\end{enumerate}
Example: let \(T\) be the lifetime in days of a product: a random varaible following an exponential distribution. Suppose the product is \(s\) days old already. Then we can express
\[P(T > s + t | T > s)\]
is the probability it will last \(t\) more days. Using our notation from Chapter 1, we can re-express this using our set algebra as
\[\frac{P((T > s + t) \cap (T > s))}{P(T > s)} = \frac{P(T > s + t)}{P(T > s)} = \frac{1 - P(T < s + t)}{1 - P(T < s)} = \frac{e^{-\lambda(s + t)}}{e^{-\lambda s}} = e^{-\lambda t}\]

\section*{The Gamma Distribution}

TODO

\end{document}
