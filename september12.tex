\documentclass{article}
\usepackage[utf8]{inputenc}

\title{STA257 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 12 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

More generally than in science, in statistics, an experiment means anything that can produce outcomes. You can think of statistics as a mathematical model for understanding chance phenomena.

On the slide, we have a graph of the chance of a jet engine failing versus time. Now, for those of you who may be getting into actuarial science, a jet engine is quite a bit like a human being. Just like a human being, a jet engine is composed of a complicated set of interconnected and interdependent pieces. And just like a jet engine, each part is liable for failure, leading to a system failure. We can draw probabilities of failure over time which go up and down, and these can help us make decisions. The grey shaded region on the graph has to do with uncertainty, which we will further discuss. Overall, such graphs can be very useful for making predictions and decisions in business: replace the engine too often and you lose money, not enough and it's unsafe.

In probability theory, an experiment is something that produces random outcomes. The set of possible outcomes is known as the \underline{sample space}.

For an example of an experiment, assume we have a very predictable student who always picks one of three streets, each of which being an outcome:
\begin{itemize}
  \item [\(\omega_1\):] Harbord
  \item [\(\omega_2\):] Wilcox
  \item[\(\omega_3\):] Russell
\end{itemize}
This gives the sample space \(\Omega = \{\omega_1, \omega_2, \omega_3\}\).

Examples of events could include:
\begin{itemize}
  \item The chosen street's name has an ``a'' in it - \(\{\omega_1\}\)
  \item The chosen street's name does \textit{not} have an ``a'' in it - \(\{\omega_2, \omega_3\}\)
  \item The chosen street intsersects Huron street - \(\{\omega_1, \omega_2, \omega_3\} = \Omega\}\)
  \item The chosen street intersects Yonge street - \(\{\} = \varnothing\)
\end{itemize}

For another example, let's make our experiment be to observe how many students attended a class. An example outcome would be, say \(\omega = 344\), and the sample space might be
\[\Omega = \{1,...,400\}\]

Some events could include
\begin{itemize}
  \item [A)] An odd number of students come to class
  \item [B)] An even number of students come to class
  \item [C)] More than 100 students come to class
\end{itemize}

\section*{Algebra of Events}

Let \(E_1, E_2\) be events and \(\Omega\) be the state space.

\begin{definition}
  The \underline{union} of two events, \(E_1 \cup E_2\), is the event that either occur or both occur. So, if each event is a set of outcomes, then the union of the events is the union of the sets.
\end{definition}
Examples:
\begin{itemize}
  \item \(A \cup B = \Omega\) (any number of students came to class)
  \item \(A \cup C = \) the number of students which came to class is even or greater than 100
\end{itemize}

\begin{definition}
  The \underline{intersection}, \(E_1 \cap E_2\), of two events is the event that both occur. So if each event is a set of outcomes, then the intersection of the events is the intersection of the sets.
\end{definition}
Examples:
\begin{itemize}
  \item \(A \cap B = \varnothing\) (the number of students which came to class is neither even or odd)
  \item \(B \cap C = \) the number of students which came to class is even and greater than 100
\end{itemize}

\begin{definition}
  The \underline{complement}, \(E_1^C = \Omega \setminus E_1\), of an event is the event that it doesn't occur, i.e. the set of outcomes in the sample space not in the event
\end{definition}
Examples:
\begin{itemize}
  \item \(A^C = B\) (not odd = even)
  \item \(B^C = A\) (not even = odd)
  \item \(C^C = \) 100 or less students came to class
\end{itemize}

\begin{definition}
  The \underline{empty set} \(\{\} = \varnothing = \Omega^C = \Omega\setminus\Omega\), is the set with no elements. Clearly, no outcomes can lead to it occuring, so it never happens
\end{definition}
Examples: \(A \cap B\) - no number can be both even or odd

\subsection*{Some Laws of Set Theory}
Let \(A, B, C\) be arbitrary sets.
\begin{itemize}
  \item Commutative: \(A \cup B = B \cup A\), \(A \cap B = B \cap A\)
  \item Associative: \((A \cup B) \cup C = A \cup (B \cup C)\)
  \item Distributive: \((A \cup B) \cap C = (A \cap C) \cup (B \cap C)\), and
  \((A \cap B) \cup C = (A \cup C) \cap (B \cup C)\).
\end{itemize}

\section*{Venn Diagrams}

Suppose we have a box representing the sample space of all things that can possibly happen in our experiment. If \(B \cap C = \varnothing\), where \(B, C\) are events, we can draw them as two separate circles that do not interact, contained within the larger sample space. The union of the two events is represented by the points in both circles. In general, when we've got two events that might overlap, we draw them as overlapping circles, with the intersection of the circles representing, well, the intersection (the union is still the points composing both circles).

What if we wanted to draw the complement of \(A\), \(A^C\)? Then, if \(A\) is a circle, the complement is just whatever's in the box and not in the circle.

For a final drawing, say we had an event \(C\) which is completely contained in another event \(A\), say \(A\) is the event that there are an even number of students, and \(C\) is the event where there are no students (0 is even). Then we draw the circle for \(C\) within the circle of \(A\), and we write this as \(C \subset A\), that is, everything in \(C\) is in \(A\), or \(C\) is a \textit{subset} of \(A\).

Note that we will not be using the \(\subseteq, \not\subseteq\) notation in this course.

\section*{Probability Measures}
\begin{definition}
  A \underline{probability measure} is a function, such as \(P(event)\) (\(P: \mathcal{P}(\Omega) \to [0, 1]\)), which takes an event as an input, and outputs a probability. The probabilities are real numbers in the interval \([0, 1]\), they are 0 for the empty set and 1 for the entire space. The three axioms of probability, not in Kolmogorov's order, are

  \begin{enumerate}
    \item Normalization: \(P(\Omega) = 1\)
    \item Nonnegativity: \(A \subset \Omega \implies P(A) \geq 0\)
    \item Additivity: if two events \(A, B\) are disjoint, then
    \[P(A \cup B) = P(A) + P(B)\]
  \end{enumerate}

  Note that the names of these axioms are not needed in this course, but are included for quick reference.
\end{definition}

\subsection*{Properties of \(P\)}

\begin{proposition}
\(P(A^C) = 1 - P(A)\)
\end{proposition}
\begin{proof}
\(P(\Omega) = P(A \cup A^C) = P(A) + P(A^C) = 1 \implies P(A^C) = 1 - P(A)\)
\end{proof}
\begin{proposition}
\(P(\varnothing) = 0\)
\end{proposition}
\begin{proof}
\(P(\Omega) = 1 = P(\Omega \cup \varnothing) = P(\Omega) + P(\varnothing) = 1 + P(\varnothing) \implies P(\varnothing) = 0\)
\end{proof}
\begin{proposition}
If \(A \subset B\), then \(P(A) \leq P(B)\)
\end{proposition}
\begin{proof}
  Let \(C = B \setminus A\).
  \[P(B) = P(A \cup C) = P(A) + P(C) \implies P(B) - P(A) = P(C) \geq 0 \implies P(B) \geq P(A)\]
\end{proof}

\section*{Computing Probabilities}

Let \(A_i\) represent the \(i^{th}\) possible event of \(N\). If all events are equally likely, then
\[P(A_1) = P(A_2) = ... = P(A_n) = \frac{1}{N}\]
Otherwise, we dig deeper and look at the outcomes comprising an event. If all outcomes are equally likely, then we can at least say that
\[P(A_i) = \frac{\text{number of ways that } A_i \text{ can occur}}{\text{total number of outcomes}}\]

If the outcomes \underline{aren't all equally likely} to occur, we can use other techniques
\begin{itemize}
  \item e.g. suppose there are \(m\) possible outcomes, \(\omega_j\)
  \item So \(|\Omega| = m\) and \(j \in \{1,...,m\}\).
  \item Let \(\mathbb{I}: \Omega \to \{0, 1\}\) represent the \textit{indicator function}
  \[\mathbb{I}(x) = \left\{\begin{array}{cccc}
    0 & \text{if} & x & \text{is false}  \\
    1 & \text{if} & x & \text{is true}
  \end{array}\right.\]
  \item Then
  \[P(A_i) = \sum_{j = 1}^m\mathbb{I}(\omega_j \in A_i)P(\{\omega_j\})\]
\end{itemize}

Suppose you have two experiments. One has \(m\) outcomes and the other has \(n\) outcomes. There are \(mn\) possible outcomes for the two experiments. This \textit{multiplication principle} extends to 3 or more experiments as well.

\section*{Permutations}
\begin{definition}
  A \underline{permutation} is some ordered arrangement of objects
\end{definition}
We're often interested in counting the number of permutations possible for a given set of objects.

Let's imagine, for example, we have \(n\) cards. We clearly have \(n\) possibilities for picking the first card. For the second card, there are \(n - 1\) probabilities. Finally, when we get to the last card, there is only 1 possible choice left. So the possible choices are
\[\prod_{i = 1}^ni = n!\]
Suppose now we only want to take \(r\) itsems from a total set of \(n\) (and the orer in which they're chosen still matters). How many ways can we do this, if we use:
\begin{itemize}
  \item Sampling without replacement:
  \[n \cdot (n - 1) \cdot ... \cdot (n - r + 1) = \prod_{i = n - r + 1}^ni\]
  How can we write this in terms of factorials? If we do the algebra, we get
  \[\frac{n!}{(n - r)!}\]
  \item Sampling with replacement:
  We have \(n\) choices \(r\) times, giving \(n^r\) possibilities.
\end{itemize}
Most people using permutations consider only sampling \underline{without} replacement, i.e. the \(r\) members in the chosen subset must be distinct.
\begin{itemize}
  \item Common notation: \(P_r^n\) denotes the number of possible choices of \(r\) elements from \(n\) elements
  \item Question: what is \(P^3_2\). We compute:
  \[\frac{3!}{(3 - 2)!} = \frac{6}{1} = 6\]
\end{itemize}

\section*{Permutations vs Combinations}

SUppose we choose \(r\) items from a total set of \(n\), but now we don't care about the order of the \(r\) items. How many such samples of size \(r\) are possible?

Again, we'll stick with sampling without replacement:
\[\frac{\text{number of ways of choosing } r \text{ items from } n}{\text{number of ordering } r \text{ items}}
= \frac{P^n_r}{r!} = \frac{n!}{r!(n - r)!}\]
The common notation for this is \({n \choose r}\), pronounced \(n\) ``choose'' \(r\).

\section*{Famous Problems from the Textbook}
\begin{enumerate}
  \item How many students should be in a classroom in order for it to be more likely than not that at least two of them share a birthday?

  \vspace{2mm}

  There's a few nice principles we can keep in mind when we're solving these questions.
  \begin{itemize}
    \item Principle 1: ``at least'': we see this term ``at least,'' and this can be a great simplification for us. Maybe there are 2 pairs of students that share a birthday, or maybe 3, and that's going to be an awful lot of probabilities to add up. But we're saved from this by the word ``at least,'' since we only care that \text{at least} 2 share a birthday, and hence can just take the complement of \text{none} of them sharing a birthday.
    \item Principle 2: Consider small, easy cases (say 2 or 3 students) and build up
  \end{itemize}
  Let's start applying Principle 2:
  \begin{itemize}

    \item [2 students:] Clearly, the probability two students share a birthday \(\frac{1}{365}\), since of the 365 possible days the second person's birthday can be, there's only one which is the first person's birthday. So there's a \(\frac{364}{365}\) chance they don't share a birthday.

    \item [3 students:] Consider the third student. If the other two students share a birthday, then two students share a birthday. If not, there's a \(\frac{363}{365}\) chance that the third student does not have a birthday in common with the other two (who have two distinct birthdays). So we have total probability
    \[\frac{364 \cdot 363}{365^2}\]

    \item [\(n\) students:]
    \[\frac{364 \cdot 363 \cdot ... \cdot (365 - n + 1)}{(365)^{n - 1}}\]
    So we want to find the \(n\) where this reaches 50\%, which happens between \(n = 22\) and \(n = 23\).

  \end{itemize}

  \item How many students must you ask to have a 50:50 chance of funding someone who shares your birthday?

  \vspace{2mm}

  Since each person you ask has a \(\frac{364}{365}\) chance of having a different birthday, the probability after asking \(n\) people is \(\frac{364^n}{365^n}\). Solving, we find
  \[\frac{364^n}{365^n} \leq \frac{1}{2} \iff 2 \cdot 364^n \leq 365^n\]
  \[\iff \log(2) + n\log(364) \leq n \cdot \log(365) \iff n(\log(365) - \log(364)) \geq \log(2) \iff n \geq \frac{\log(2)}{\log(365) - \log(364)} = ~252.65\]
  The lowest integer satisfying this is 253

\end{enumerate}

\section*{Grouping \(n\) objects into \(r\) classes}
Suppose you have \(n\) objects which you want to sort into \(r\) diferrent classes. \(\forall i \in \{1,...r\}\), let \(n_i\) be the number of objects in the \(i^{th}\) class. For example, if \(n = 6\) and \(r = 3\), you might have \(n_1 = 0, n_2 = 4, n_3 = 2\).

The number of ways to achieve a specific ordered set of \(n_i\)'s, such as \(\{0, 4, 2\}\), is
\[\frac{n!}{n_1!n_2!...n_r!}\]
which is sometimes written as
\[{n \choose n_1n_2...n_r}\]
This is \textit{not} the same as \(n\) choose \(n_1n_2...n_r\). When you're getting assesed on these things, it'll be made clear from the problem what is meant.

Exercise: try arriving at this formula.

\section*{Conditional Probability}

We now have an understanding of what ``the probability of \(A\) means'' A conditional probability, however, changes that probability of \(A\) to something else.
\begin{definition}
  \(P(A|B)\) is read as ``the probability of \(A\) given \(B\)'' and can be defined as
  \[P(A|B) := \frac{P(A \cap B)}{P(B)}\]
  assuming \(P(B) \neq 0\).
\end{definition}

\begin{itemize}
  \item It's also very useful to rearrange the equation to
  \[P(A \cap B) = P(A|B)P(B)\]
  \item Using the property of commutativity covered near the start of lecture, assuming \(P(A) \neq 0\),
  \[P(A \cap B) = P(A|B)P(B) = P(B \cap A) = P(B|A)P(A)
  \implies \frac{P(A)}{P(B)} = \frac{P(A|B)}{P(B|A)}\]
  or
  \[P(A|B) = \frac{P(B|A)P(A)}{P(B)}\]
  This is called Bayes' Rule.
\end{itemize}

\section*{Bayes' Rule}
\[P(A|B) = \frac{P(B|A)P(A)}{P(B)}\]
Assume we've got a window and we can see whether it's cloudy or not, but we can't see anything movable to tell us whether the weather is windy or not. We go to our meteorologist friend and find out the probability the weather is cloudy is \(P(C) = 0.1\) and the probability that it's windy is \(P(W) = 0.2\), and if it's windy, then the probability that it's cloudy is \(0.4\). We can then compute that the probability that it's windy, given it's cloudy, is
\[P(W|C) = \frac{P(C|W)P(W)}{P(C)} = \frac{0.4 \cdot 0.2}{0.1} = \frac{0.08}{0.1} = 0.8\]

\section*{The Law of Total Probability}
Let \(B_1, B_2,..., B_n\) be such that
\[\bigcup_{i = 1}^nB_i = \Omega\]
and \(i \neq j \implies B_i \cap B_j = \varnothing\) with \(\forall i, P(B_i) > 0\). Then, for any event \(A\),
\[P(A) = \sum_{i = 1}^nP(A | B_i)P(B_i)\]
This means that if you divide a sample space into a set of disjoint \(B_i\)'s, then any probability \(P(A)\) can be expressed as the sum of the conditional probabilities of \(A\)-given-\(B_i\), each weighted by \(P(B_i)\).

\subsection*{Examples of the Law}
You're an actuary looking at data across three cities:

\[
\begin{array}{ccc}
  \text{City} & \text{Babies born last year} & \text{Probability of reaching age 80} \\ \hline
  C_1 & 4500 & 0.72 \\
  C_2 & 6300 & 0.78 \\
  C_3 & 11200 & 0.64
\end{array}
\]
We can find the probability that a baby from any of the three cities to reach age 80 is
\[0.72\frac{4500}{22000} + 0.78\frac{6300}{22000} + 0.64\frac{11200}{22000} = ~0.697\]

\end{document}
