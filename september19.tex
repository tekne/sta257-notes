\documentclass{article}
\usepackage[utf8]{inputenc}

\title{STA257 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 19 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\newtheorem*{notation}{Notation}

\begin{document}

\maketitle

Note: since the instructor has provided mostly complete slides during the lecture, many of the sentences here have been taken directly from the slides since there was little time to paraphrase.

\section*{Independence}

Rememeber in this course we expanded our notion of the word experiment, experiment means a lot more in this course than it usually does. Well, symmetrically, we're contracting our notion of independence: independence means a lot less in this course. In this course, when we say independence, we mean stochastic independence, which is quite a narrow definition, reproduced below.

\begin{definition}
Two events \(A\) and \(B\) are \underline{independent} if \[P(A \cap B) = P(A)P(B)\]
An alternative way of expressing independence is
\[P(A) = P(A | B) \land P(B) = P(B | A)\]
The only problem with this is that this second definition only works when \(P(A), P(B) \neq 0\). If we can assume this then this alternative characterization can really help.
\end{definition}
\textit{Pairwise independence} doesn't guarantee \textit{mutual independence}. For example, consider the events \(A = \) you pass all courses, \(B = \) Malala passes all courses, \(C = \) exactly one of you does so. If we assume \[P(A) = P(B) = \frac{1}{2} \implies P(C) = \frac{1}{2}\] then these probabilities become pairwise independent:
\[P(A \cap C) = P(A)P(C) = P(B \cap C) = P(B)P(C) = P(A \cap B) = P(A)P(B) = \frac{1}{4}\]

\begin{definition}
If events \(A_1, A_2, A_n\) are \underline{mutually independent}, then for any combination,
\[P(A_1 \cap ... \cap A_n) = P(A_1)...P(A_n)\]
\end{definition}

\subsection*{Question from Professor Banjevic}

A regular tetrahedron is painted with three colors
\begin{itemize}
  \item The first side is painted blue
  \item The second is painted yellow
  \item The third is painted red
  \item The fourth side is painted all three colors
\end{itemize}

Rolling the tetrahedron, consider the events:

\begin{itemize}
  \item \(R = \) red appears at base
  \item \(B = \) blue appears at base
  \item \(Y = \) yellow appears at base
\end{itemize}

Consider the following questions

\begin{itemize}

  \item Are these events pairwise independent? Yes:
  \[\forall E \in \{R, B, Y\}, P(E) = \frac{1}{4} + \frac{1}{4} = \frac{1}{2},\]
  \[\forall E' \in \{R, B, Y\}, P(E \cap E') = \frac{1}{4} = P(E)P(E')\]

  \item Are these events mutually independent? No:
  \[P(R \cap B \cap Y) = \frac{1}{4} \neq P(R)P(B)P(Y) = \frac{1}{8}\]

\end{itemize}

This concludes chapter 1 of our book. I think it's pretty good at setting up the methods we'll use in our course, though it is a little harsh on Bayesian methods. Take with a pinch of salt the sort of dismissive, suspicious attitude the author has in chaper one towards Bayes.

In the unit two slides, I stuck on the end, if you downloaded them yesterday, you might have missed this, a few slides about proof. This is because one or two of you have been asking for more help with the proofs. There is also a proof thread on Piazza.

\section*{Introduction to Random Variables}

\underline{Random variables} are essentially random numbers. More precisely, a random variable is a function whose domain is a sample space, and whose range is a set of real numbers. They are of two types
\begin{itemize}
  \item Discrete: can take on only a finite or countably infinite number of values
  \item Continuous: can take on an uncountably infinite number of values
\end{itemize}
Examples of discrete random variables include
\begin{enumerate}
  \item The number of students who showed up to class today
  \item The net migration at all Canada-US border crossings today
  \item The number of heads observed after flipping a coin 10 times
  \item How many customers a restaurant serves between 5 and 6:30
  \item The luminisity of a traffic signal, measured in candelas, where
  \[\{500 = \text{ red}, 600 = \text{ yellow}, 700 = \text{ green}\}\]
\end{enumerate}
Recall the experiment of our classmate walking to school, which had three possible outcomes.

A \underline{probability measure} maps subsets of the sample space to a real number with patricular characteristics. The number is a probability between 0 and 1, e.g.
\[P(\{\omega_1 = \text{ the student took Wilcox street}) = \frac{1}{3}\]

The probability mass function (pmf), a.k.a. frequency function, is the probability measure associated with a random variable.

Probability mass functions are one of two kinds of "probability distributions" we'll look at this week and the next. It's called a mass function for reasons that will become clear next week when we talk about density.

The \underline{cumulative} distribution function (cdf) comes from summing a pmf up to the point \(x\). It is sometimes referred to as \(F(x)\):
\[F(x) = P(X \leq x), x \in \reals\]

\subsection*{Notation}
\begin{enumerate}
  \item We tend to use uppercase letters for random variables in general and lowercase for specific values in their range.
  \item Often \(P(X = x)\) is abbreviated to \(p(x)\)
  \item Some authors use \(Pr(.)\) instead of \(P(.)\), and similar
\end{enumerate}

\subsection*{Independence}

Two discrete random variables \(X, Y\) are independent if for all \(x\) that \(X\) can be,
\[P(X = x \text{ and } Y = y) = P(X = x)P(Y = y)\]

\section*{Bernoulli Random Variables}

The simplest example of a discrete random variable is the \underline{Bernoulli random variable}. It can take on only two values: 0 or 1, which probabilities \(1 - p\) and \(p\) respectively. This is described by it's pmf:
\[p(1) = p, p(0) = 1 - p, p(x) = 0 \text{ if } x \neq 0 \land x \neq 1\]
Remember, we're using the abbreviations \(P(X = 1) = p(1)\), etc.

Tip: you'll often hear the term "Bernoulli trial," where trial means one run of an experiment. So a Bernoulli trial is running an experiment that has just two possible outcomes, like flipping a coin.

In this course, we'll follow the common (but not universal) practice of requiring \(p \in (0, 1)\). This allows us to write
\[p(x) = \left\{\begin{array}{cc}
  p^{x}(1 - p)^{1 - x} & \text{if } x = 0 \lor x = 1 \\
  0 & \text{otherwise}
\end{array}\right.\]

\subsection*{Examples}
Flipping a fair coin (i.e. \(p = 0.5\)), let \(x = 0\) represent tails and \(x = 1\) represent heads.
\[p(0) = 0.5^0(1 - 0.5)^1 = 0.5\]
\[p(1) = 0.5^1(1 - 0.5)^0 = 0.5\]

\section*{The Binomial Distribution}
\begin{notation}
  \(X = 0\) is often referred to as a \underline{failure}, and \(X = 1\) as a success. For this week, then, \(p\) will be called the probability of success.
\end{notation}

Suppose multiple (\(n\)) Bernoulli trials are run. Then the probability that the total is \(k\) is
\[p(k) = {n \choose k}p^k(1 - p)^{n - k}\]
But why is this called the binomial distribution?

A binomial is the sum of two terms, e.g. \(a + b, x + 2x^2\) or \(x + 2\). If you raise a binomial to the \(n^{th}\) power and expand,
\[(a + b)^n = \sum_{k = 0}^n{n \choose k}a^kb^{n - k}\]
For this reason, the combinatorial term \({n \choose k}\) is called the \underline{binomial coefficient}. It can take on this name whether or not you use \({n \choose k}\) in the context of binomials.

If we let \(a = p\) be the probability of success in a Bernoulli trial, and let \(b = 1 - p\) be the probability of failure, the right hand side above is a series corresponding to the pmf of the binomial distribution.

\subsection*{Examples}

Suppose our student from the previous example, over our 12 week course, walks to class once each week. What is the probability that they enter the university via Harbord Street (\(p = 1/6\)) exactly twice?
\[p(2) = {12 \choose 2}\frac{1}{6^2}\frac{5^{10}}{6^{10}} = {12 \choose 2}\frac{5^{10}}{6^{12}} = ~0.296\]
Now suppose, for example, a jar has ten black candies and five white candies. "Sampling with replacement," you take a candy at random and put it back, seven times. What is the pmf for \(k\), the number of black candies sampled?

You can use a computer, for example, with a \textbf{for}-loop, 8 times, to see what the probability for each \(k\) from 0 to 7, using the formula
\[p(k) = {7 \choose k}\frac{2^k}{3^7}\]

\subsection*{Using the cdf of the binomial distribution}
It's often handy to use the cdf of the binomial distribution, denoted sometimes by \(B(k; n, p)\).

For example, suppose you're the employee responsible for replacing lightbulbs in an office building with 1200 fixtures. If each bulb has a 99\% chance of working when you encounter it, the chances that no more than 20 bulbs will need replacing is
\[B(20; 1200, 0.01) = \sum_{k = 0}^{20}p(k) \approx 98.9\%\]
Alternatively, there are some nice, quick approximations. However, different approximations apply in different situations (i.e. \(p \ll 1\)), and we won't cover them in this course.

\section*{The Geometric Distribution}

Suppose multiple Bernoulli trials are run, but we don't specify \(n\) ahead of time. We keep going, instead, until the first success (\(X = 1\)) occurs. This might be after \(k = 1\), \(k = 4\), \(k = 314\), etc. It depends on luck and on the value of \(p\) governming each Bernoulli trial.

What's the probability that the total number of trials is \(k\)?
\[p(k) = (1 - p)^{k - 1}p\]
where \(k > 0\) is an integer.

\begin{notation}
  It's common to replace \(n\) with \(k\) hereare we have done above. All our distributions this week will be expressed \(p(k)\), but \(k\) can take on different meanings.
\end{notation}

Question: does
\[\sum_{k = 0}^\infty p(k) = 1\]
Intuitively, this is true, and we can prove it by using the formula to sum an infinite geometric series:
\[\sum_{k = 0}^\infty(1 - p)^{k - 1} = \frac{1}{p} \implies \sum_{k = 0}^\infty p(k) = p \cdot \frac{1}{p} = 1\]

Why, however, is it called the geometric distribution?

Recall that in a geometric sequence, elements are found by multiplying the previous element by a fixed number called the common ratio, e.g.
\begin{itemize}
  \item \(4, 20, 100, 500...\) has common ratio 5
  \item \(1, \frac{1}{2}, \frac{1}{4}, ...\) has common ratio \(\frac{1}{2}\)
\end{itemize}
Here, we're dealing with a geometric sum of probabilities.

\subsection*{Example}

Suppose that during our 12 week course, the midterm is in week 7. If she comes to school once per week, what is the probability that the first time your classmate enters the university via Russel Street (\(p = 1/3\)) is on the day of the midterm?
\[p(7) = \frac{2^6}{3^6}\frac{1}{3} = \frac{2^6}{3^7} = 0.03\]

\section*{The Negative Binomial Distribution}

The geometric distribution describes a specific case of a more general situation: the probability that it takes \(k\) trials in total until \(r\) successes appear, which is
\[p(k) = {k - 1 \choose r - 1}p^r(1 - p)^{k - r}\]
This distribution can be derived by considering the chance of having \(r\) successes (\(p^r\)), \(k - r\) failures (\(p^{k - r}\)), which can be ordered in \({k - 1 \choose r - 1}\) ways.

In the case of the geometric distribution, \(r = 1\) (check this). Note, furthermore, that the definitions of both these definitions vary depending on the textbook, website or library. In this lecture, we use John Rice's definition and notation for consistency.

\subsection*{Example}

Suppose again that, during our 12 week course with the midterm in week 7, what's the probability that the \textit{second} time your classmate enters the university via Russell street (\(p = 1/3\)) is on the midterm?
\[p(2) = {6 \choose 2}\frac{1}{3^2}\frac{2^{5}}{3^{5}} = 0.22\]
For another example, suppose your niece has ten boxes of Girl Guide cookies to sell. Assume that at each house in her neighborhood, there's a 40\% chance of selling one box and a 60\% chance of selling none. How many houses might she need to visit? We can graph \(p(k)\) versus \(k\) to get an idea.

\section*{The Hypergeometric Distribution}

So far today, we've only sampled with replacement. The calculations have been simplified by assuming a constant value of \(p\) across independent Bernoulli trials.

What if, however, we sample without replacement, from a jar, say, with \(n\) candies, \(r\) of which are black. If we draw \(m\) candies, the probability that \(k\) of them are black is
\[p(k) = \frac{{r \choose k}{n - r \choose m - k}}{{n \choose m}}\]
This is called the \underline{hypergeometric distribution}. This is because there are \({r \choose k}\) ways to choose \(k\) black candies, \({n - r \choose m - k}\) ways to choose \(m - k\) candies which are not black, divided by \({n \choose m}\) ways to choose \(m\) candies.

\subsection*{Example}
In the game of Bridge, you're dealt 13 cards. How likely are you to receive two spades?
\begin{itemize}
  \item Method 1 (from Week 1): TODO
  \item Method 2 (from Week 2): we have \(n = 52, r = 13, m = 13\), giving
  \[p(2) = \frac{{13 \choose 2}{39 \choose 11}}{{52 \choose 13}} = \frac{130732371432}{635013559600} \approx 0.206\]
\end{itemize}

\section*{The Poisson Distribution}

Imagine the \textit{limit} of a binomial distribution as \(n \to \infty\) and \(p \to 0\) in such a way that \(np = \lambda\) is held constant. Specifically, suppose \(X\) is a random variable equal to the number of times some phenomenon occurs in a given interval of time. Now divide that interval into many (\(n\)) subintervals of equal length, such that the probabilitya phenomenon occurs in each subinterval is very small. We've assumed
\begin{enumerate}
  \item What happens in one subinterval is independent of what happens in the otherwise\item The probability of the phenomenon's occurence is the same (\(p\)) in each subinterval
  \item The phenomena don't occur simultaneously
\end{enumerate}
The third assumption is feasible if... TODO

We've described a very special case of the binomial distribution. When we taker \(n \to \infty\) and \(p \to 0\), after some algebra we can rewrite this special binomial distributio as
\[p(k) = \frac{\lambda^k}{k!}e^{-\lambda}, k \in \{0,1, 2, ...\}\]
This is the \underline{Poisson distribution}. Recall \(\lambda = np\), so it's some positive real number. It isknown as the \textit{intensity parameter}, \textit{rate parameter}, \textit{event rate}, etc.

To see how we could derive this, we begin by writing out the binomial distribution:
\[p(k) = {n \choose k}p^k(1 - p)^k\]
Since \(n \neq 0\), we can write \(\lambda = np \iff p = \frac{\lambda}{n}\). We can then start taking the limit:
\[\lim_{n \to \infty}\frac{\lambda^k}{n^k}\left(1 - \frac{\lambda}{n}\right)^k = \lim_{n \to \infty}{n \choose k}\frac{\lambda^k}{k!}\frac{n!}{n^k(n - k)!}\left(1 - \frac{\lambda}{n}\right)^{n - k}\]
Looking at the left, we see part of our goal expression, and to the right,
\[\lim_{n \to \infty}\left(1 - \frac{\lambda}{n}\right)^n = e^{-\lambda}\]
So we can rewrite the above as
\[\frac{\lambda^k}{k!}e^{-\lambda}\lim_{n \to \infty}\cancelto{1}{\frac{n!}{n^k(n - k)!}}\cancelto{1}{\left(1 - \frac{\lambda}{n}\right)^{-k}}\]

Note that, when \(n\) gets very large, the binomial distribution looks \textit{kind of} like the Poisson distribution.

Rules of thumb for when we can reliably use the possion apporximation to a binomial distribution are \(np \geq 1\) and \(n \geq 20, p < 0.05\) or \(n \geq 100, p < 0.1\).

So the parent of the Poisson distribution is the binomial distribution, for which we struggled to find a good cumulative distribution function. Unfortunately, the Poisson distribution's cdf is a similar summation:
\[F(k) = e^{-\lambda}\sum_{i = 0}^k\frac{\lambda^i}{i!}\]

\section*{The Poisson Process}

Let's consider a common application of the Poisson distribution: suppose \(S\) is an interval of time you're interested in studying (e.g. 7 to 10 PM) with duration denoted \(|S|\) (e.g. 180 minutes).

Let \(S_1, S_2,..., S_n\) be disjoint subsets of \(S\) (e.g. 7 to 7:15, 7:15 to 7:30,...). We can then denote the occurence counts of a phenomenon in these subintervals by \(N_1, N_2,..., N_n\).

In a \underline{Poisson process}, the \(N_i\) are independent random variables that follow Poisson distributions with parameters \(\lambda|S_1|, \lambda|S_2|, ..., \lambda|S_n|\).

In other words, the expected number of occurences \(N_i\) in some subinterval \(S_i\) is given by
\[P(N_i = k) = \frac{(\lambda|S_i|)^k}{k!}e^{-\lambda|S_i|}\]

\subsection*{Other Applications of the Poisson distribution}
The Poisson distribution and process are not only used to describe phenomena over time. They can be used to describe phenomena over space as well. The parameter \(\lambda\) can be interpreted as the average number of events in any unit of \textit{extent}, such as volume, area, length, time, etc., including combinations of them.

\end{document}
